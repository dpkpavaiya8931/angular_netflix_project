import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor( private router: Router) {

  }

  routeToLogin(){
    this.router.navigate(['login'])
  }

  routeToDashboard(){
    this.router.navigate(['home']);
  }

  routeToWatchMovie(){
    this.router.navigate(['watchMovie']);
  }

  routeToProfile(){
    this.router.navigate(['profile']);
  }

}
