import { Component, OnInit } from '@angular/core';
import { TrendingService } from '../trending.service';
import{ SharedService } from '../services/shared.service';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { AuthService } from '../auth.service';
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService:AuthService,private apollo:Apollo,private sharedService:SharedService,private trendService : TrendingService) { 
  }
  
  trend;
  nowplaying;
  results;
  favMovies:any;
  ngOnInit() {
    this.sharedService.hideAddFav = false;
    this.sharedService.hideRemFav = true;
    this.sharedService.userActive= false;
    this.trend = this.trendService.getTrending()
    .subscribe(data =>{
      this.trend=data['results'];
    })
    this.nowplaying = this.trendService.getNowPlaying()
    .subscribe(test=>{
      this.nowplaying=test['results'];
    })


    this.getFavList();
    
  }


  // --------------------------------Getting All Fav Movies List function-----------------------------------------------
  getFavList(){
    this.apollo.mutate({
      mutation: gql`
      mutation{
        getFavList(email:"${this.authService.getEmail()}"){
          id,
          title,
          poster_path,
          vote_average
        }
      }`      
    }).subscribe(result=>{
      console.log(result.data['getFavList']);
      this.sharedService.setFavList(result.data['getFavList']);
      this.favMovies = result.data['getFavList'];
      if(Object.keys(this.favMovies).length == 0){
        this.sharedService.notShow = true;
      }
    });    
  }

}
