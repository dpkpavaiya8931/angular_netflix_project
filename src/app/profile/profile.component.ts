import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  msg:String;

  constructor(private modalService: NgbModal,private sharedService:SharedService,private authService:AuthService) { }

  ngOnInit(): void {
    this.sharedService.userActive = false;
    this.sharedService.mobile = this.authService.getMobile();
    let paid = this.authService.getPaidUser();
    if(paid === "yes"){
      this.msg = "Premium Account";
      this.sharedService.premiumLink = true;
    }else{
      this.msg = "Get Premium Account";
      this.sharedService.premiumLink = false;
    }
  }

  openBackDropCustomClass(content) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
  }

}
