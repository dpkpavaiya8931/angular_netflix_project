import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AfficheFilmComponent } from './holly-mov/holly-mov.component';
import { AfficheSerieComponent } from './bolly-mov/bolly-mov.component';
import { DetailFilmComponent } from './hollywood/hollywood.component';
import { DetailSerieComponent } from './bollywood/bollywood.component';
import { SearchComponent } from './search/search.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { SignupComponent } from './signup/signup.component';
import { CanActivateGuard } from './can-activate.guard';
import {ChangePassComponent} from './change-pass/change-pass.component'
import { WatchMovieComponent } from './watch-movie/watch-movie.component';
import { PaymentComponent } from './payment/payment.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
// import { AuthenticationGuard } from './a';


const routes: Routes = [
  { path:'login', component: LoginComponent },
  { path: 'resetPassword', 
    component:ResetpasswordComponent,
    canActivate:[CanActivateGuard]
  },
  { path:'signup', component: SignupComponent},
  { path:'profile',
    component: ProfileComponent,
    canActivate:[CanActivateGuard]
  },
  { path: '', redirectTo:'/login',pathMatch:'full' },
  { 
    path:'home',
    component:HomeComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'hollywood',
    component:AfficheFilmComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'bollywood',
    component:AfficheSerieComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'hollypage/:id',
    component:DetailFilmComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'bollypage/:id',
    component:DetailSerieComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'search/:find',
    component:SearchComponent,
    canActivate:[CanActivateGuard]
  },
  {
    path:'passchange',
    component:ChangePassComponent,
    canActivate:[CanActivateGuard]
  },
  { 
    path:'watchMovie',
    component:WatchMovieComponent,
    canActivate:[CanActivateGuard]
  },
  {
    path:'payment',
    component:PaymentComponent,
    canActivate:[CanActivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
