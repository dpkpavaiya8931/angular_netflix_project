import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SerieService {

  constructor(private http:HttpClient) { }
  getSeries():Observable<any[]>{
    return this.http.get<any[]>('https://api.themoviedb.org/3/discover/movie?api_key=87dfa1c669eea853da609d4968d294be&language=hi-IN&region=IN&sort_by=popularity.desc&page=1&primary_release_year=2018&with_original_language=hi')
    .pipe(
      tap(data => data)
    )
  }
  getSerieByName(name): Observable<any>{
    return this.http.get<any>('https://api.themoviedb.org/3/movie/'+name+'?api_key=87dfa1c669eea853da609d4968d294be&language=hi-IN')
    .pipe(
      tap(data => data)
    )
  }
}
