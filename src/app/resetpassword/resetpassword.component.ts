import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { RouterService } from '../router.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  msg:String;

  constructor(private apollo:Apollo,private routerService:RouterService) { }

  ngOnInit(): void {
  }

  resetPasswordForm = new FormGroup({
    oldPassword: new FormControl(''),
    newPassword: new FormControl('',[Validators.minLength(8)]),
  });

  reset(resetPasswordForm){
    let x = document.getElementById("snackbar");
    if(!(resetPasswordForm.value.oldPassword)){
      this.msg="Please enter Your Current Password";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(!(resetPasswordForm.value.newPassword)){
      this.msg="Please enter New Password";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(resetPasswordForm.value.newPassword.length < 8){
      this.msg="New Password Length Must be 8";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(resetPasswordForm.value.oldPassword === resetPasswordForm.value.newPassword){
      this.msg="Old and New Password Must be Different";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else{

      this.apollo.mutate({
        mutation: gql`
        mutation{
          resetPassword(email:"venkat@gmail.com",
            oldPassword:"${resetPasswordForm.value.oldPassword}",
            newPassword:"${resetPasswordForm.value.newPassword}"
          ){
            result
          }
        }`
      }).subscribe(result=>{
        if(result.data["resetPassword"].result == 1){
          this.msg="Password Changed Sucessfully";
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          this.routerService.routeToProfile();
        }else{
          this.msg="Error Please try again later";
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
      })

    }
  }

}
