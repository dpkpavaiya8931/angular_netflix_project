import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  userActive:boolean = true;
  api:string ="";
  email:String;
  username:String;
  mobile:String;
  notShow:boolean;
  hideAddFav:boolean;
  hideRemFav:boolean = true;
  favMovies;
  premiumLink:boolean;
  constructor() { }


  setFav(movies:any){
    localStorage.setItem('favMovie',movies);
  }

  getFav(){
    return localStorage.getItem('favMovie')
  }

  setFavList(movies:any){
    localStorage.setItem('list',movies);
  }

  getFavList(){
    return localStorage.getItem('list');
  }

}
