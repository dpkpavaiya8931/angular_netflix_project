import { Component, OnInit } from '@angular/core';
import { FormGroup,FormGroupDirective, FormBuilder,Validators, FormControl } from '@angular/forms'
import { AuthService } from '../auth.service';
import { RouterService } from '../router.service'

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  msg:String = "";
  // Get the snackbar DIV

  constructor(private sharedService:SharedService,private httpLink:HttpLink,private apollo:Apollo,private authService:AuthService,private routerService:RouterService) {
    // apollo.create({
    //   link:this.httpLink.create({uri:'http://localhost:8010/api/v1/user'}),
    //   cache: new InMemoryCache()
    // })
  }
  
  signupForm = new FormGroup({
    name: new FormControl('',[Validators.minLength(8)]),
    email: new FormControl('',[Validators.email]),
    password: new FormControl('',[Validators.minLength(6)]),
    mobile: new FormControl('',[Validators.minLength(10),Validators.pattern(/0-9/)])
  });

  ngOnInit(): void {
    this.sharedService.userActive = true;
    this.sharedService.api = "user";
  }

  register(signupform: FormGroup){
    let x = document.getElementById("snackbar");
    console.log(this.signupForm.value);
    if(this.signupForm.value.name === ""){
      this.msg = "Name is Mandatory Field";
      // Add the "show" class to DIV
      x.className = "show";
      // After 3 seconds, remove the show class from DIV
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.name.length < 6){
      this.msg="Name length must be 6";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.email === ""){
      this.msg="Email is Mandatory Field";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.password === ""){
      this.msg="Password id Mandatory Field";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.password.length < 6){
      this.msg="Password length must be 6";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.mobile === ""){
      this.msg="Mobile is Mandatory Field";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.signupForm.value.mobile.length < 10){
      this.msg="Mobile Length must be 10";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else{
      
      this.apollo.mutate({
        mutation: gql`
        mutation {register(userInput: {
          email:"${this.signupForm.value.email}",
          username : "${this.signupForm.value.name}",
          password : "${this.signupForm.value.password}",
          mobile : "${this.signupForm.value.mobile}",
          paid : "no",
          admin:"no"
        }){
          admin,
          paid
        }
        }`
      }).subscribe(result=>{
        this.routerService.routeToLogin();
      });
      
    }
  }
}