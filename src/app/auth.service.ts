import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {

   }

  authenticatedUser(loginuser:any){
     return this.http.post('http://localhost:3000/auth/v1',loginuser);
  }
  
  setBearerToken(token:string){
     localStorage.setItem('bearerToken',token)
  }
  getBearerToken(){
     return localStorage.getItem('bearerToken');
  }

  setAdmin(admin:string){
     localStorage.setItem('admin',admin);
  }

  getUsername(){
    return localStorage.getItem('username');
  }

  setUsername(username:string){
    localStorage.setItem('username',username);
  }

  getEmail(){
    return localStorage.getItem('email');
  }

  setEmail(email:string){
    localStorage.setItem('email',email);
  }

  getMobile(){
    return localStorage.getItem('mobile');
  }

  setMobile(mobile:string){
    localStorage.setItem('mobile',mobile);
  }

  getAdmin(){
   return localStorage.getItem('admin');
  }

  setPaidUser(paid:string){
     localStorage.setItem('paid',paid);
  }

  getPaidUser(){
     return localStorage.getItem('paid');
  }

  isUserAuthenticated(){
    let token = this.getBearerToken();
    if(!token){
      return false;
    }else{
      return true;
    }
  }

}
