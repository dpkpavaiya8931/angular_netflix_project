import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { SearchService } from '../search.service';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../auth.service';
import { RouterService } from '../router.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  // userActive:boolean = true;
  constructor(private routerService:RouterService,private authService:AuthService,private sharedService:SharedService,private searchService: SearchService, private router: Router, private route: ActivatedRoute) { }
  onSubmit(f) {
    console.log('param : ' + f)
    location.reload();
    this.router.navigate(['search/' + f.value.search])
      .then(() => {
        window.location.reload();
      });
  }

  ngOnInit() {
    this.sharedService.username = this.authService.getUsername();
    this.sharedService.email = this.authService.getEmail();
  }

  logoClick(){
    console.log("working");    
  }

  signout(){
      localStorage.clear();
      this.routerService.routeToLogin();
  }

  profilePage(){
    this.routerService.routeToProfile();
  }

}
