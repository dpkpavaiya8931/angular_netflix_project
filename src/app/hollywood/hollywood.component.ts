import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmService } from '../hollywood.service';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../auth.service';
import { RouterService } from '../router.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

@Component({
  selector: 'app-detail-film',
  templateUrl: './hollywood.component.html',
  styleUrls: ['./hollywood.component.css']
})
export class DetailFilmComponent implements OnInit {

  id:number;
  film;
  msg:String;
  favMoviesList;


  constructor(private apollo:Apollo,private modalService: NgbModal,private routerService:RouterService,private authService:AuthService,private route: ActivatedRoute,private filmService: FilmService,private sharedService:SharedService) {
    this.route.params
    .subscribe( params => this.id = params.id)
   }

  ngOnInit() {
    this.getFilmByName(this.id);
    this.sharedService.userActive = false;
    this.getFav(this.id);
  }
  
  getFilmByName(id){
    this.filmService.getFilmByName(id)
    .subscribe(data => {
     this.film = data;
    });
  }

  openBackDropCustomClass(content) {
    let paid = this.authService.getPaidUser();
    if(paid ==="yes"){
      this.routerService.routeToWatchMovie();
    }else{
      this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
    }
  }


  //-------------------------------Add to Favourite List Functionality-------------------------------------------
  addToFav(film){
    let x = document.getElementById("snackbar");
    this.apollo.mutate({
      mutation: gql`
      mutation{
        addFav(favMovie:{
          id:${film.id},
          title:"${film.title}",
          poster_path:"${film.poster_path}",
          vote_average:"${film.vote_average}",
          email:"${this.authService.getEmail()}"
        }){
          result
        }
      }`      
    }).subscribe(result=>{
      if(result.data["addFav"].result == 1){
        this.sharedService.hideAddFav = true;
        this.sharedService.hideRemFav = false;
      }else{
        this.msg="Failed to add in Watch Later List";
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
      }
    });    
  }


//-------------------------------Remove From Favourite List Functionality-------------------------------------------
  remFav(film){
    let x = document.getElementById("snackbar");
    this.apollo.mutate({
      mutation: gql`
        mutation{
          remFav(id:${film.id},email:"${this.authService.getEmail()}") {
            result
          }
        }`      
    }).subscribe(result=>{
      if(result.data["remFav"].result == 1){
        this.sharedService.hideAddFav = false;
        this.sharedService.hideRemFav = true;
      }
    });    
  }


  //-------------------------------Movie is Fav or Not Functionality-------------------------------------------
  getFav(id){
    this.apollo.mutate({
      mutation: gql`
        mutation{
          getFav(id:${id},email:"${this.authService.getEmail()}") {
            result
          }
        }`      
    }).subscribe(result=>{
      if(result.data["getFav"].result == 1){
        this.sharedService.hideAddFav = true;
        this.sharedService.hideRemFav = false;
      }else{
        this.sharedService.hideAddFav = false;
        this.sharedService.hideRemFav = true;
      }
    });    
  }
  
}
