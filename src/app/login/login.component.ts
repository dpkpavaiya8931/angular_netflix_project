import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';

import gql from 'graphql-tag';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AuthService } from '../auth.service';
import { RouterService } from '../router.service';

import{ SharedService } from '../services/shared.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  x = document.getElementById("snackbar");
  msg:String="";

  constructor(private sharedService:SharedService,private httpLink:HttpLink,private apollo:Apollo,private authService:AuthService,private routerService:RouterService) {
    
  }

  loginForm = new FormGroup({
    email: new FormControl('',[Validators.email]),
    password: new FormControl('',[Validators.minLength(8)]),
  });

  

  ngOnInit(){
    this.sharedService.userActive = true;
    this.sharedService.api = "user";
  }

  validateUser(loginForm){
    let x = document.getElementById("snackbar");
    if(!(loginForm.value.email)){
      this.msg="Please enter your Email ID";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(!(loginForm.value.password)){
      this.msg="Please Enter your Password";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else{
      this.apollo.watchQuery({
        query : gql`{
          login(loginUser:{
            email : "${loginForm.value.email}",
            password:"${loginForm.value.password}"
          }){
            paid,
            token,
            email,
            username,
            mobile
          }
        }`
      })
      .valueChanges.subscribe(result => {
        // console.log(result.data["login"].username);
        this.sharedService.username = result.data["login"].username;
        this.sharedService.mobile = result.data["login"].mobile;
        this.authService.setBearerToken(result.data["login"].token);
        this.authService.setPaidUser(result.data["login"].paid);
        this.authService.setEmail(result.data["login"].email);
        this.authService.setUsername(result.data["login"].username);
        this.authService.setMobile(result.data["login"].mobile);
        if(result.data["login"].token){
            this.routerService.routeToDashboard();
        }
      });
    }
  }
}
