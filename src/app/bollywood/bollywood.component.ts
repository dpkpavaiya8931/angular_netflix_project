import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SerieService } from '../bollywood.service';

@Component({
  selector: 'app-detail-serie',
  templateUrl: './bollywood.component.html',
  styleUrls: ['./bollywood.component.css']
})
export class DetailSerieComponent implements OnInit {

  id:number;
  serie;
  constructor(private route: ActivatedRoute,private serieService: SerieService) {
    this.route.params
    .subscribe( params => this.id = params.id)
   }
  ngOnInit() {
    this.getFilmByName(this.id);
  }
  getFilmByName(id){
    this.serieService.getSerieByName(id)
    .subscribe(data => {
     this.serie = data;
     console.log(this.serie);
     
    });
  }

}
