import { Component, OnInit } from '@angular/core';
import { FilmService } from '../hollywood.service';

@Component({
  selector: 'app-affiche-film',
  templateUrl: './holly-mov.component.html',
  styleUrls: ['./holly-mov.component.css']
})
export class AfficheFilmComponent implements OnInit {

  constructor(private filmService : FilmService) { }
  films;
  results;
  ngOnInit() {
    this.films = this.filmService.getFilm()
    .subscribe(data =>{
      this.films=data['results'];
    })
  }

}
