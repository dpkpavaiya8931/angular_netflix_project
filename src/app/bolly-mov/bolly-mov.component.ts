import { Component, OnInit } from '@angular/core';
import { SerieService } from '../bollywood.service';

@Component({
  selector: 'app-affiche-serie',
  templateUrl: './bolly-mov.component.html',
  styleUrls: ['./bolly-mov.component.css']
})
export class AfficheSerieComponent implements OnInit {

  
  constructor(private serieService : SerieService) { }
  series;
  results;
  ngOnInit() {
    this.series = this.serieService.getSeries()
    .subscribe(data =>{
      this.series=data['results'];
    })
}
}
