import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RouterService } from '../router.service';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../auth.service';


interface Card {
  value: string;
  viewValue: string;
}
interface Banks{
  value:string;
}

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  msg:String = "";
  selectedValue: string;
  cards: Card[] = [
    {value: 'CredirCard-0', viewValue: 'CreditCard'},
    {value: 'DebitCard-1', viewValue: 'DebitCard'}
  ];
  select:string;
  banks: Banks[] = [
    {value: 'SBI Bank'},
    {value: 'Indian Bank'},
    {value: 'Andhra Bank'},
    {value: 'Karoor Vaysya Bank'},
    {value: 'ICICI Bank'},
    {value: 'Bank Of Baroda'},
    {value: 'Canara Bank'},
    {value: 'Union Bank'},
    {value: 'HDFC Bank'},
    {value: 'Axis Bank'},
    {value: 'Kotak Mahindra Bank'}

  ]
   paymentForm = new FormGroup({
    email: new FormControl('',[Validators.email]),
    name: new FormControl('',[Validators.minLength(6)]),
    card: new FormControl('',[Validators.required]),
    bank: new FormControl('',[Validators.required   ]),
    cardNumber: new FormControl('',[Validators.required,Validators.pattern(/0-9/)]),
    cvvNumber : new FormControl('',[Validators.required,Validators.pattern(/0-9/)])

  });

  constructor(private authService:AuthService,private sharedService:SharedService, private router:RouterService,private apollo:Apollo) { 
  }

  ngOnInit(): void {
    this.sharedService.userActive = false;
  }

  pay(paymentForm:FormGroup){
    let x = document.getElementById("snackbar");
    if(this.paymentForm.value.name  === ""){
      this.msg = "Name is Mandatory Field";
      // Add the "show" class to DIV
      x.className = "show";
      // After 3 seconds, remove the show class from DIV
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.paymentForm.value.name.length < 6){
      this.msg="Name length must be atleast 6";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }else if(this.paymentForm.value.cardNumber === ""){
      this.msg="Card Number is Mandatory Field";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    else if(this.paymentForm.value.cardNumber.length <16){
      this.msg="Card Number must be 16";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    else if(this.paymentForm.value.cvvNumber === ""){
      this.msg="CVV Number is Mandatory Field";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    else if(this.paymentForm.value.cvvNumber.length <3){
      this.msg="CVV Number must be 3 digits";
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    else{
      this.apollo.mutate({
        mutation:gql`mutation{
          updatePayment(email:"${this.authService.getEmail()}",payment:"yes"){
            result
          }
        }`
      }).subscribe(res=>{
        // console.log(res.data["updatePayment"].result);
        if(res.data["updatePayment"].result == 1){
          this.authService.setPaidUser("yes");
          this.router.routeToDashboard();
        }
      })
    }
  }

}
