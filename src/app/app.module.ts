import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AfficheFilmComponent } from './holly-mov/holly-mov.component';
import { AfficheSerieComponent } from './bolly-mov/bolly-mov.component';
import { HomeComponent } from './home/home.component';
import { DetailFilmComponent } from './hollywood/hollywood.component';
import { DetailSerieComponent } from './bollywood/bollywood.component';
import { SearchComponent } from './search/search.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule} from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatFormFieldModule} from '@angular/material/form-field';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { SignupComponent } from './signup/signup.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import {ApolloModule, APOLLO_OPTIONS, Apollo} from 'apollo-angular';
import {IgxDropDownModule} from 'igniteui-angular'

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { CanActivateGuard } from './can-activate.guard';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { SharedService } from './services/shared.service';
import { WatchMovieComponent } from './watch-movie/watch-movie.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaymentComponent } from './payment/payment.component';
import { ProfileComponent } from './profile/profile.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
 

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AfficheFilmComponent,
    AfficheSerieComponent,
    HomeComponent,
    DetailFilmComponent,
    DetailSerieComponent,
    SearchComponent,
    LoginComponent,
    SignupComponent,
    ChangePassComponent,
    WatchMovieComponent,
    PaymentComponent,
    ProfileComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    MatButtonModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatListModule,
    MatPaginatorModule,
    MatSortModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatGridListModule,
    MatSelectModule,
    LayoutModule,
    MatSidenavModule,
    MatMenuModule,
    MatButtonToggleModule,
    BrowserAnimationsModule,
    ApolloModule,
    HttpLinkModule,
    IgxDropDownModule,
    NgbModule
  ],
  providers: [CanActivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private apollo:Apollo,private httpLink:HttpLink,private sharedService:SharedService){
    var path = "user"
    var url = `http://localhost:8010/api/v1/${path}`
    apollo.create({
      link:this.httpLink.create({uri:url}),
      cache: new InMemoryCache()
    })
  }
}
